package com.example.sanjar.lesson34.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sanjar.lesson34.R;
import com.example.sanjar.lesson34.adapters.Connect;
import com.example.sanjar.lesson34.adapters.StudentAdapter;
import com.example.sanjar.lesson34.database.DBHelper;
import com.example.sanjar.lesson34.models.StudentsModel;

import java.util.ArrayList;



/**
 * Created by SANJAR on 09.02.2018.
 */

@SuppressLint("ValidFragment")
public class PageFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static int mPage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        if (getArguments() != null) {
            mPage=getArguments().getInt(ARG_PAGE);
            Toast.makeText(getContext(), mPage+"", Toast.LENGTH_SHORT).show();
        }

        RecyclerView recyclerView = view.findViewById(R.id.list);
        Connect connect=new Connect(getContext(),recyclerView);
        connect.onLoad();
        return view;
    }
    public static PageFragment getInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
