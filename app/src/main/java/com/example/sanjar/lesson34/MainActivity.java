package com.example.sanjar.lesson34;

import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.sanjar.lesson34.adapters.PagerAdapter;
import com.example.sanjar.lesson34.adapters.StudentAdapter;
import com.example.sanjar.lesson34.database.DBHelper;
import com.example.sanjar.lesson34.models.StudentsModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private ArrayList<String> data;
    private PagerSlidingTabStrip tabStrip;
    public static PagerAdapter adapterPager;
    public ArrayList<StudentsModel> studentsModels;
    public StudentAdapter adapter1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        studentsModels=new ArrayList<>();
        viewPager=findViewById(R.id.viewPager);
        loadData();
        adapterPager=new PagerAdapter(getSupportFragmentManager(),data);
        tabStrip=findViewById(R.id.tabs);
        viewPager.setAdapter(adapterPager);
        tabStrip.setViewPager(viewPager);
   }

    private void loadData() {
        data=new ArrayList<>();
        data.add("Tab 1");
        data.add("Tab 2");
        data.add("Tab 3");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.plus,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.add){
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Add student");
            View dialogView=LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog,null);
            builder.setCancelable(false);
            builder.setView(dialogView);
            final EditText name=dialogView.findViewById(R.id.firstName);
            final EditText lastName=dialogView.findViewById(R.id.lastName);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    StudentsModel studentsModel=new StudentsModel(0,name.getText().toString(),lastName.getText().toString(),0);
                    DBHelper.getHelper(MainActivity.this).insertStudent(studentsModel);
                    studentsModels.add(studentsModel);
                    Log.d(studentsModels.size()+"","***************");
                    adapterPager.notifyDataSetChanged();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }
}