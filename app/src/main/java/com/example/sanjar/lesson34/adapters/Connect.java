package com.example.sanjar.lesson34.adapters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.sanjar.lesson34.MainActivity;
import com.example.sanjar.lesson34.database.DBHelper;
import com.example.sanjar.lesson34.models.StudentsModel;

import java.util.ArrayList;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;
import static com.example.sanjar.lesson34.MainActivity.adapterPager;
import static com.example.sanjar.lesson34.fragments.PageFragment.mPage;

/**
 * Created by SANJAR on 10.02.2018.
 */

public class Connect {
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<StudentsModel> studentsModels;
    private StudentAdapter adapter;

    public Connect(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public void onLoad() {
        if (mPage == 0) {
            studentsModels = DBHelper.getHelper(context).getOneStatusStudents();
            adapter = new StudentAdapter(studentsModels);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
            setUpItemTouchHelper(mPage,studentsModels);
        }

        if (mPage == 1) {
            studentsModels = DBHelper.getHelper(context).getTwoStatusStudents();
            adapter = new StudentAdapter(studentsModels);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
            setUpItemTouchHelper(mPage, studentsModels);
        }
        if (mPage == 2) {
            studentsModels = DBHelper.getHelper(context).getThreeStatusStudents();
            adapter = new StudentAdapter(studentsModels);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
            setUpItemTouchHelper(mPage,studentsModels);
        }
    }

    private void setUpItemTouchHelper(int k, final ArrayList<StudentsModel> studentsModel) {
        if (k == 0) {
            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }
                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int swipedPosition = viewHolder.getAdapterPosition();
                    adapter = (StudentAdapter) recyclerView.getAdapter();
                    DBHelper.getHelper(context).updateStudent(new StudentsModel(studentsModels.get(swipedPosition).getId(), studentsModels.get(swipedPosition).getFirstName(), studentsModels.get(swipedPosition).getLastName(), 1));
                    studentsModel.remove(swipedPosition);
                    adapterPager.notifyDataSetChanged();
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        } else if (k == 1) {
            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }
                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int swipedPosition = viewHolder.getAdapterPosition();
                    if (direction == ItemTouchHelper.LEFT) {
                        adapter = (StudentAdapter) recyclerView.getAdapter();
                        DBHelper.getHelper(context).updateStudent(new StudentsModel(studentsModels.get(swipedPosition).getId(), studentsModels.get(swipedPosition).getFirstName(), studentsModels.get(swipedPosition).getLastName(), 0));
                        studentsModel.remove(swipedPosition);
                        adapterPager.notifyDataSetChanged();
                    }
                    else if (direction == ItemTouchHelper.RIGHT) {
                        adapter = (StudentAdapter) recyclerView.getAdapter();
                        DBHelper.getHelper(context).updateStudent(new StudentsModel(studentsModels.get(swipedPosition).getId(), studentsModels.get(swipedPosition).getFirstName(), studentsModels.get(swipedPosition).getLastName(), 2));
                        studentsModel.remove(swipedPosition);
                        adapterPager.notifyDataSetChanged();
                    }
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        }
        else if(k==2){
            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int swipedPosition = viewHolder.getAdapterPosition();
                    adapter = (StudentAdapter) recyclerView.getAdapter();
                    DBHelper.getHelper(context).updateStudent(new StudentsModel(studentsModels.get(swipedPosition).getId(), studentsModels.get(swipedPosition).getFirstName(), studentsModels.get(swipedPosition).getLastName(), 1));
                    studentsModel.remove(swipedPosition);
                    adapterPager.notifyDataSetChanged();
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        }
    }
}
