package com.example.sanjar.lesson34.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sanjar.lesson34.R;
import com.example.sanjar.lesson34.models.StudentsModel;

import java.util.ArrayList;

/**
 * Created by SANJAR on 10.02.2018.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder>{
    private ArrayList<StudentsModel> studentsData;
    public StudentAdapter(ArrayList<StudentsModel> data) {
        this.studentsData = data;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_students,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StudentsModel d = studentsData.get(position);
        holder.firstName.setText(d.getFirstName());
        holder.lastName.setText(d.getLastName());
    }

    @Override
    public int getItemCount() {
        return studentsData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView firstName;
        TextView lastName;

        public ViewHolder(View itemView) {
            super(itemView);
            firstName=itemView.findViewById(R.id.first_name);
            lastName=itemView.findViewById(R.id.last_name);
        }
    }
}
