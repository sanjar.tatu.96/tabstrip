package com.example.sanjar.lesson34.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.sanjar.lesson34.models.StudentsModel;
import java.util.ArrayList;

/**
 * Created by Sherzodbek on 22.12.2017. (Lesson19)
 */

public class DBHelper extends com.example.sanjar.lesson34.libs.DBHelper {
    @SuppressLint("StaticFieldLeak")
    private static DBHelper helper;

    public static DBHelper getHelper(Context context) {
        if (helper == null) {
            helper = new DBHelper(context);
        }
        return helper;
    }

    private DBHelper(Context context) {
        super(context, "fragment.db");
    }

    public ArrayList<StudentsModel> getStudents() {
        ArrayList<StudentsModel> students = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM students", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            students.add(new StudentsModel(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("first_name")),
                    cursor.getString(cursor.getColumnIndex("last_name")),
                    cursor.getInt(cursor.getColumnIndex("status"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return students;
    }
    public ArrayList<StudentsModel> getOneStatusStudents() {
        ArrayList<StudentsModel> students = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM students WHERE status='0'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            students.add(new StudentsModel(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("first_name")),
                    cursor.getString(cursor.getColumnIndex("last_name")),
                    cursor.getInt(cursor.getColumnIndex("status"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return students;
    }
    public ArrayList<StudentsModel> getTwoStatusStudents() {
        ArrayList<StudentsModel> students = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM students WHERE status='1'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            students.add(new StudentsModel(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("first_name")),
                    cursor.getString(cursor.getColumnIndex("last_name")),
                    cursor.getInt(cursor.getColumnIndex("status"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return students;
    }
    public ArrayList<StudentsModel> getThreeStatusStudents() {
        ArrayList<StudentsModel> students = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM students WHERE status='2'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            students.add(new StudentsModel(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex("first_name")),
                    cursor.getString(cursor.getColumnIndex("last_name")),
                    cursor.getInt(cursor.getColumnIndex("status"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return students;
    }
    private ContentValues getStuentsData(StudentsModel studentsModel) {
        ContentValues values = new ContentValues();
        values.put("first_name", studentsModel.getFirstName());
        values.put("last_name", studentsModel.getLastName());
        values.put("status", studentsModel.getStatus());
        return values;
    }
    public void insertStudent(StudentsModel studentsModel) {
        mDatabase.insert("students", null, getStuentsData(studentsModel));
    }
    public void updateStudent(StudentsModel studentsModel) {
        mDatabase.update("students", getStuentsData(studentsModel), "id=" +studentsModel.getId(), null);
    }
    public void deleteStudent(StudentsModel studentsModel){
        mDatabase.delete("students","id="+studentsModel.getId(),null);
    }
}
