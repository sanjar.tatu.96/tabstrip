package com.example.sanjar.lesson34.models;

/**
 * Created by SANJAR on 10.02.2018.
 */

public class StudentsModel {
    private int id;
    private String firstName;
    private String lastName;
    private int status;

    public StudentsModel() {
    }

    public StudentsModel(int id, String firstName, String lastName, int status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
