package com.example.sanjar.lesson34.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sanjar.lesson34.fragments.PageFragment;

import java.util.ArrayList;

/**
 * Created by SANJAR on 09.02.2018.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> dataPage;

    public PagerAdapter(FragmentManager fm, ArrayList<String> data) {
        super(fm);
        this.dataPage = data;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.getInstance(position);
    }

    @Override
    public int getCount() {
        return dataPage.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dataPage.get(position);
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }
}
